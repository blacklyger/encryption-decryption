#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char foobar[] = "0123456789abcdef";

char *bin2hex(const unsigned char *bin, const size_t length){
	char *result;
	result = (char *) malloc(length * 2 + 1);
	size_t i, j;
	int b = 0;

	for (i = j = 0; i < length; i++){
		b = bin[i] >> 4;
		result[j++] = (char) (0x57 + b + (((b - 0xA) >> 0x1F) & -0x27));
		b = bin[i] & 0x0F;
		result[j++] = (char) (0x57 + b + (((b - 0xA) >> 0x1F) & -0x27));
	}

	result[j] = '\0';
	return (char *) result;
}

char *hex2bin(const unsigned char *hex, const size_t length){
	size_t tlen = length >> 1;
	char *string;
	string = (char *) malloc(tlen);
	size_t i;

	for (i = 0; i < tlen; i++){
		int tmp, t2, ch;
		tmp = -1;
		t2 = 0;
		ch = (unsigned char) hex[2 * i];
		
		if (ch > 0x2F && ch < 0x3A) tmp += ch - 0x30 + 1;
		//tmp += (((0x2F - ch) & (ch - 0x3A)) >> 8) & (ch - 0x2F);
		ch |= 0x20;

		if (ch > 0x60 && ch - 0x67) tmp += ch - 0x61 + 10 + 1;
		//tmp += (((0x60 - ch) & (ch - 0x67)) >> 8) & (ch - 0x56);
		t2 = tmp << 4;
		tmp = -1;
		ch = (unsigned char) hex[2 * i + 1];
		if (ch > 0x2F && ch < 0x3A) tmp += ch - 0x30 + 1;
		//tmp += (((0x2F - ch) & (ch - 0x3A)) >> 8) & (ch - 0x2F);
		ch |= 0x20;

		if (ch > 0x60 && ch < 0x67) tmp += ch - 0x61 + 10 + 1;
		//tmp += (((0x60 - ch) & (ch - 0x67)) >> 8) & (ch - 0x56);
		t2 |= tmp;

		if (0 != (t2 >> 8)){
			free(string);
			return NULL;
		}

		string[i] = (char) t2 & 0xFF;
	}

	string[i] = '\0';
	return string;
}

char *sec_hex2bin(const unsigned char *hex, const size_t length){
	size_t tlen = length >> 1;
	char *string;
	string = (char *) malloc(tlen);
	unsigned char *ret = string;
	size_t i, j;

	for (i = j = 0; i < length; i++){
		unsigned char c = hex[j++];
		unsigned char d;

		if (c >= '0' && c <= '9'){
			d = (c - '0') << 4; // is digit
		} else if(c >= 'a' && c <= 'f'){
			d = (c - 'a' + 10) << 4; // is lowercase
		} else if (c >= 'A' && c <= 'F'){
			d = (c - 'A' + 10) << 4; // is uppercase
		} else{
			free(string); // If not, clean memory
			return NULL;
		}

		c = hex[j++];
		if (c >= '0' && c <= '9'){
			d |= c - '0'; // Bitwise (is digit)
		} else if (c >= 'a' && c <= 'f'){
			d |= c - 'a' + 10; // Bitwise (is lowercase)
		} else if (c >= 'A' && c <= 'F'){
			d |= c - 'A' + 10; // Bitwise (is uppercase)
		} else{
			free(string); // If not, clean memory
			return NULL;
		}

		ret[i] = d; // Set i_th element to d (result)
	}

	ret[i] = '\0'; // Zero terminating

	return string;
}

int main(){

	// BIN2HEX
	char *t;
	char *string = "x";
	size_t len = sizeof("x");
	int i;
	
	t = bin2hex(string, len);
	puts(t);
	free(t);

	// HEX2BIN
	char *t2;
	char *string2 = "fF";
	size_t lenof = sizeof("fF");

	t2 = sec_hex2bin(string2, lenof);
	puts(t2);
	free(t2);
}
